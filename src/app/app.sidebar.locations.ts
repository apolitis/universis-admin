export const APP_SIDEBAR_LOCATIONS = [
  {
    name: 'Home',
    key: 'Sidebar.Dashboard',
    url: '/',
    icon: 'fa fa-home',
    index: 0
  },
  {
    name: 'Users',
    key: 'Sidebar.Users',
    url: '/admin/users',
    icon: 'fas fa-user',
    index: 0
  },
  {
    name: 'Groups',
    key: 'Sidebar.Groups',
    url: '/admin/groups',
    icon: 'fas fa-user-friends',
    index: 0
  }
];
