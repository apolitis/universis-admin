import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { User, ObjectState } from './admin.interfaces';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private context: AngularDataContext) {
   }

   createUser(user: User): Promise<User> {
     // forcibly set user for insert
     Object.assign(user, <ObjectState> {
       $state: 1
     });
    return this.context.model('Users').save(user);
   }

   updateUser(user: User): Promise<User> {
     // forcibly set user for update
     Object.assign(user, <ObjectState> {
       $state: 2
     });
    return this.context.model('Users').save(user);
   }

   getUser(username: string): Promise<User> {
    return this.context.model('Users').where('name').equal(username).getItem();
   }

}
