import { NgModule } from '@angular/core';
import { AdminComponent } from './admin.component';
import { TablesModule } from '@universis/tables';
import { UserListComponent } from './user-list/user-list.component';
import { AdminRoutingModule } from './admin-routing.module';
@NgModule({
  imports: [
    TablesModule,
    AdminRoutingModule
  ],
  declarations: [AdminComponent, UserListComponent],
  exports: [AdminComponent]
})
export class AdminModule { }
