import { Component, OnInit } from '@angular/core';
import {UserListConfiguration} from '../../assets/tables/users/list';
import { AdvancedTableConfiguration } from '@universis/tables';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: []
})
export class UserListComponent implements OnInit {

  public readonly tableConfiguration:any = AdvancedTableConfiguration.cast(UserListConfiguration);
  public filter: any = {};

  constructor() { }

  ngOnInit() {

  }

}
