/*
 * Public API Surface of admin
 */
export * from './lib/admin.interfaces';
export * from './lib/admin.service';
export * from './lib/admin.component';
export * from './lib/admin.module';
export * from './lib/admin-routing.module';
export * from './lib/user-list/user-list.component';
